data:extend(
  {
    {
      type = "custom-input",
      name = "beltplanner_abort",
      key_sequence = "",
	  linked_game_control = "toggle-menu",
      consuming = "none",

    },
	{
      type = "custom-input",
      name = "beltplanner_clean",
      key_sequence = "",
	  linked_game_control = "clean-cursor",
      consuming = "none",
    },
	
    {
      type = "custom-input",
      name = "beltplanner_create_ghosts",
      key_sequence = "ALT + G",
      consuming = "none",

    },
    {
      type = "custom-input",
      name = "beltplanner_undo",
      key_sequence = "ALT + Z",
      consuming = "none",
    }
  }
)
