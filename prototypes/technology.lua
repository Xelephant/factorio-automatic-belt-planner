data:extend({
    {
        type = "technology",
        name = "beltplanner",
        order = "c-k-b-a",
        icon = "__beltplanner-patched-18__/graphics/technology/beltplanner.png",
        icon_size = 128,
        prerequisites =
        {
            "logistics",
        },
        unit =
        {
            count = 100,
            ingredients =
            {
                {"automation-science-pack", 1},
                {"logistic-science-pack", 1},
            },
            time = 30,
        },
    },
})
