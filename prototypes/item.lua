data:extend({
    {
        type = "item-with-label",
        name = "beltplanner",
        subgroup = "tool",
        order = "c[automated-construction]-c[beltplanner]",
        icon = "__beltplanner-patched-18__/graphics/item/beltplanner.png",
        icon_size = 32,
        place_result = "beltplanner",
        stack_size = 1,
        can_be_mod_opened = true,
    },
})
