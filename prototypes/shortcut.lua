data:extend(
{
  {
    type = "shortcut",
    name = "give-beltplanner",
	action = "lua",
    order = "give-beltplanner",
    localised_name = {"shortcut.give-beltplanner"},
	technology_to_unlock = "beltplanner",
	style = "green",
    icon =
    {
      filename = "__beltplanner-patched-18__/graphics/icons/PlannerIcon32px.png",
      priority = "extra-high-no-scale",
      size = 32,
      position = position,
      scale = 1,
      flags = {"icon"}
    },
    small_icon =
    {
      filename = "__beltplanner-patched-18__/graphics/icons/PlannerIcon24px.png",
      priority = "extra-high-no-scale",
      size = 24,
      scale = 1,
      flags = {"icon"}
    },
    disabled_icon =
    {
      filename = "__beltplanner-patched-18__/graphics/icons/PlannerIcon32pxoff.png",
      priority = "extra-high-no-scale",
      size = 32,
      scale = 1,
      flags = {"icon"}
    },
    disabled_small_icon =
    {
      filename = "__beltplanner-patched-18__/graphics/icons/PlannerIcon24pxoff.png",
      priority = "extra-high-no-scale",
      size = 24,
      scale = 1,
      flags = {"icon"}
    },
  }
})